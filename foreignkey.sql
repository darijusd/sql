create table parduotuves (
	ParduotuvesID int primary key auto_increment,
    Adresas varchar(100)
);
create table kasos (
	KasosID int primary key auto_increment,
	Numeris int,
    ParduotuvesID int,
    foreign key (ParduotuvesID) references parduotuves(ParduotuvesID)
);
